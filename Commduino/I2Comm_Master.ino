#include <Wire.h>

/*
Connect 2 Arduino-type devices together with 3 pins: GND, SDA and SCL:
Uno, Pro Mini, Nano, Lilypad: A4 (SDA), A5 (SCL)
Mega, Due: 20 (SDA), 21 (SCL)
Leonardo, Yun: 2 (SDA), 3 (SCL)
ESP8266: D2 (SDA), D1 (SCL)
 */ 

int state = 0;

void setup() {
  // Start the I2C Bus as Master
  Wire.begin(); 
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}
void loop() {
  state = digitalRead(LED_BUILTIN);
  Serial.println(state);
  digitalWrite(LED_BUILTIN, !state);   // turn the LED on (HIGH is the voltage level)
  Wire.beginTransmission(9); // transmit to device #9
  Wire.write(state);              // sends x 
  Wire.endTransmission();    // stop transmitting
  Serial.print(F("Sent State: "));
  Serial.println(state);
  delay(random(100, 5000));
}
