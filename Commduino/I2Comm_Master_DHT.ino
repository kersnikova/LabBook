#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

/*
Connect 2 Arduino-type devices together with 3 pins: GND, SDA and SCL:
Uno, Pro Mini, Nano, Lilypad: A4 (SDA), A5 (SCL)
Mega, Due: 20 (SDA), 21 (SCL)
Leonardo, Yun: 2 (SDA), 3 (SCL)
ESP8266: D2 (SDA), D1 (SCL)
 */

#define DHTPIN 2     // Digital pin connected to the DHT sensor 
// Uncomment the type of sensor in use:
//#define DHTTYPE    DHT11     // DHT 11
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)
DHT_Unified dht(DHTPIN, DHTTYPE);
uint32_t delayMS;

int hum = 0;
int temp = 0;

void setup() {
  // Start the I2C Bus as Master
  Wire.begin(); 
  Serial.begin(9600);
  // Initialize device.
  dht.begin();
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  dht.humidity().getSensor(&sensor);
  delayMS = sensor.min_delay / 1000;
}
void loop() {
  delay(delayMS);
  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);
    Serial.print(F("Temperature: "));
    Serial.print(event.temperature);
    temp = event.temperature;
    Serial.println(F("°C"));
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
    Serial.print(F("Humidity: "));
    Serial.print(event.relative_humidity);
    hum = event.relative_humidity;
    Serial.println(F("%"));
  
  Serial.print(F("Sent Temperature: "));
  Serial.println(temp);
  Serial.print(F("Sent Humidity: "));
  Serial.println(hum);
  Serial.println(F("************"));
  
  Wire.beginTransmission(9); // transmit to device #9
  Wire.write(temp);              // sends temp
  Wire.write(hum);              // sends humidity
  Wire.endTransmission();    // stop transmitting
}
