// include program library for servo motors
#include <Servo.h>

// start servo motors protocols
Servo myservo;

// define where the moisture sensor is connected
const int moistureSensor = A0;

// defines the desired humidity threshold, from 0 to 1024 (0-100%), used in line 37
int humThreshold = 500;
// define time measurement interval, in milliseconds: 1000 * 60 = 1 min, * 60 = 1 h, used in line 49
int interval = 1000 * 5;
// watering time, more time - more water, used in line  41
int wateringTime = 1000 * 2;


// storage for humidity threshold, used in line 31
int humVaulue;

void setup() {
// begin serial communication at baud 9600
  Serial.begin(9600);
// start servo motor at pin 9
  myservo.attach(9); 
// move servo to 0 degress, this position open, for attaching the tube to the servo arm unplug and stop it now
  myservo.write(0);
// wait 2 seconds
  delay(1000 * 2); 
}
void loop() {
// read and print moisture value
  humVaulue = (analogRead(moistureSensor));
  Serial.println(humVaulue);

// read, compare and execute
  if (humVaulue < humThreshold) {
// turn servo to position 179 degress and start watering
    myservo.write(179);
// wait while watering
    delay(wateringTime);
// turn servo to position 1 degree and stop watering
    myservo.write(1);
  }
// if humThreshold isn't reached stay at position 1 degree
  else myservo.write(1);

// wait between mesuring and acting
    delay(interval);
}
