#include "DHT.h"
#define DHTTYPE DHT22

int redPinH = 11;
int greenPinH = 10;
int bluePinH = 9;

int redPinT = 3;
int greenPinT = 5;
int bluePinT = 6;

#define DHTPIN A0
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(redPinH, OUTPUT);
  pinMode(greenPinH, OUTPUT);
  pinMode(bluePinH, OUTPUT); 
  pinMode(redPinT, OUTPUT);
  pinMode(greenPinT, OUTPUT);
  pinMode(bluePinT, OUTPUT);
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println(" °C "); // DHT11 sampling rate is 1HZ.
    
    if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
    }
    delay(500);

  if (h < 50){
  analogWrite(greenPinH, map(h, 30, 50, 0, 255));
  analogWrite(redPinH, map(h, 0, 50, 255, 0));
  } else {
  analogWrite(greenPinH, map(h, 50, 80, 255, 0));
  analogWrite(bluePinH, map(h, 50, 100, 0, 255));
  }
  
  if (t < 23){
  analogWrite(greenPinT, map(t, 20, 23, 0, 255));
  analogWrite(bluePinT, map(t, 0, 23, 255, 0));
  } else {
  analogWrite(greenPinT, map(t, 23, 26, 255, 0));
  analogWrite(redPinT, map(t, 23, 35, 0, 255));
  }
}